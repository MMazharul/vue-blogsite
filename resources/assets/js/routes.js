
import adminhome from './components/admin/AdminHome.vue'
import categorylist from './components/admin/category/List.vue'
import AddCategory from './components/admin/category/new.vue'
import editCategory from './components/admin/category/Edit.vue'
import postList from './components/admin/post/list.vue'
import postCreate from './components/admin/post/New.vue'

// fronend component

import publicHome from './components/public/PublicHome.vue'
import blogpost from './components/public/blog/BlogPost.vue'
import singlepost from './components/public/blog/SinglePost.vue'

export const routes = [
     { path: '/home', component: adminhome },
     { path: '/category-list', component: categorylist },
     { path: '/add-category', component: AddCategory },
     { path: '/category/:id/edit', component: editCategory },
     { path: '/post-list', component: postList },
     { path: '/post/create', component: postCreate },

    // frontend routing

     { path: '/', component: publicHome },
     { path: '/blog', component: blogpost },
     { path: '/singlepost/:id', component: singlepost },
     { path: '/categories/post/:id', component: blogpost },



]
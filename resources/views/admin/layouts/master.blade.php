<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('ui/backend/')}}/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body class="app sidebar-mini rtl">

<div id="app">
    @include('admin/layouts/partials/header')
    {{--@include('admin/layouts/partials/sidebar')--}}
    <adminmain></adminmain>
</div>



<!-- Navbar-->


<!-- Essential javascripts for application to work-->
<script src="{{asset('ui/backend/')}}/js/jquery-3.2.1.min.js"></script>
<script src="{{asset('ui/backend/')}}/js/popper.min.js"></script>
<script src="{{asset('ui/backend/')}}/js/bootstrap.min.js"></script>
<script src="{{asset('ui/backend/')}}/js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
{{--<script src="{{asset('../../ui/backend/')}}js/plugins/pace.min.js"></script>--}}
<!-- Page specific javascripts-->
<script type="text/javascript" src="{{asset('../../ui/backend/js/plugins/chart.js')}}"></script>

{{--<script type="text/javascript">--}}
    {{--var data = {--}}
        {{--labels: ["January", "February", "March", "April", "May"],--}}
        {{--datasets: [--}}
            {{--{--}}
                {{--label: "My First dataset",--}}
                {{--fillColor: "rgba(220,220,220,0.2)",--}}
                {{--strokeColor: "rgba(220,220,220,1)",--}}
                {{--pointColor: "rgba(220,220,220,1)",--}}
                {{--pointStrokeColor: "#fff",--}}
                {{--pointHighlightFill: "#fff",--}}
                {{--pointHighlightStroke: "rgba(220,220,220,1)",--}}
                {{--data: [65, 59, 80, 81, 56]--}}
            {{--},--}}
            {{--{--}}
                {{--label: "My Second dataset",--}}
                {{--fillColor: "rgba(151,187,205,0.2)",--}}
                {{--strokeColor: "rgba(151,187,205,1)",--}}
                {{--pointColor: "rgba(151,187,205,1)",--}}
                {{--pointStrokeColor: "#fff",--}}
                {{--pointHighlightFill: "#fff",--}}
                {{--pointHighlightStroke: "rgba(151,187,205,1)",--}}
                {{--data: [28, 48, 40, 19, 86]--}}
            {{--}--}}
        {{--]--}}
    {{--};--}}
    {{--var pdata = [--}}
        {{--{--}}
            {{--value: 300,--}}
            {{--color: "#46BFBD",--}}
            {{--highlight: "#5AD3D1",--}}
            {{--label: "Complete"--}}
        {{--},--}}
        {{--{--}}
            {{--value: 50,--}}
            {{--color:"#F7464A",--}}
            {{--highlight: "#FF5A5E",--}}
            {{--label: "In-Progress"--}}
        {{--}--}}
    {{--]--}}

    {{--var ctxl = $("#lineChartDemo").get(0).getContext("2d");--}}
    {{--var lineChart = new Chart(ctxl).Line(data);--}}

    {{--var ctxp = $("#pieChartDemo").get(0).getContext("2d");--}}
    {{--var pieChart = new Chart(ctxp).Pie(pdata);--}}
{{--</script>--}}
<!-- Google analytics script-->
<script type="text/javascript">
    if(document.location.hostname == 'pratikborsadiya.in') {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-72504830-1', 'auto');
        ga('send', 'pageview');
    }
</script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
        {{--<!doctype html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
    {{--<meta charset="UTF-8">--}}
    {{--<meta name="viewport"--}}
          {{--content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">--}}
    {{--<meta http-equiv="X-UA-Compatible" content="ie=edge">--}}
    {{--<title>Document</title>--}}
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
{{--</head>--}}
{{--<body>--}}
{{--<div id="app">--}}
    {{--<h1>Hi</h1>--}}

    {{--<home></home>--}}
{{--</div>--}}
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
{{--</body>--}}
{{--</html>--}}
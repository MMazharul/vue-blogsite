<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class BlogController extends Controller
{
    public function getBlogPosts()
    {
        $blogposts = Post::with('user','category')->orderBy('id','desc')->get();
        return response()->json([
            'blogposts'=>$blogposts
        ],200);
    }


    public function latestCategories()
    {
        $latestCategory = Category::orderBy('id','desc')->get();
        return response()->json([
            'latestCategory'=>$latestCategory
        ],200);
    }

    public function singlePost($id)
    {
        $singlePost=Post::with('user','category')->where('id',$id)->first();
        return response()->json($singlePost);
    }

    public function categoryPosts($id)
    {

       $categoryPosts=Post::with('user','category')->where('cat_id',$id)->get();
        return response()->json($categoryPosts);
    }



    public function searchPost()
    {
        $search=Input::get('search');

        $searchPosts=Post::with('user','category')
            ->where('title','like',"%$search%")
            ->orWhere('description','like',"%$search%")
            ->get();

        return response()->json($searchPosts);
    }
}

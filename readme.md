<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

#Md Mazharul Islam

# About Laravel vue js Blog Project

![laravel vue js vuex vue router blog project](https://user-images.githubusercontent.com/29582239/49328894-e5dff680-f5a1-11e8-9190-c6b25730bfb5.png)
   
<article class="markdown-body entry-content" itemprop="text">
  <ul>
  <li> Clone the repository with git clone </li>
   <li>Copy .env.example file to .env and edit database credentials there</li>
  <li> Run composer install</li>
  <li> Run php artisan key:generate</li>
  <li> Run php artisan migrate</li>
  <li> Run npm install</li>
   <li>Run npm run dev</li>
   </ul>
<b>That's it - load the homepage.</b>
</article>